<script lang="ts">
	import { onMount } from 'svelte';
	import { browser } from '$app/environment';

	import { format } from 'date-fns';
	import { formatInTimeZone } from 'date-fns-tz/esm';

	import { arc } from 'd3-shape';

	let date = new Date(Date.now());
	$: time12 = format(date, 'h:mm');
	$: time24 = format(date, 'HH:mm');
	$: timeUTC = formatInTimeZone(date, 'Etc/UTC', 'HH:mm');
	$: timeUTCSeconds = formatInTimeZone(date, 'Etc/UTC', 'ss');

	let timerStart = 0;
	let timerRunning = false;
	$: timer = timerRunning === true ? Math.floor((date.getTime() - timerStart) / 1000) : 0;
	$: timerDisplay = timer < 60 ? timer : Math.floor(timer / 60);
	$: timerUnits = timer < 60 ? 'sec.' : 'min.';

	let intervalLength = 15 * 60;
	$: intervalsPassed = Math.floor(timer / intervalLength);

	/**
	 * Start the notification timer and set interval
	 * @param notificationInterval {number}: interval for notifications, in seconds
	 */
	function startTimer(notificationInterval: number) {
		Notification.requestPermission();

		timerStart = Date.now() - 1000; // - 1000: handle gap between timer and setInterval (below)
		intervalLength = notificationInterval;
		timerRunning = true;
	}
	/**
	 * Stop the notification timer
	 */
	function stopTimer() {
		timerRunning = false;
	}

	$: fifteenMinuteTimerActive = timerRunning && intervalLength === 900;
	$: fiveMinuteTimerActive = timerRunning && intervalLength === 300;

	let icon: SVGElement;

	const minuteArcGen = arc();
	$: minuteArc = minuteArcGen({
		innerRadius: 0,
		outerRadius: 110,
		startAngle: 0,
		endAngle: ((Math.PI * 2) / 60) * (date.getMinutes() % 60)
	});

	function getMarkNormal(numerator: number, denominator: number) {
		return [
			Math.cos(((2 * Math.PI) / denominator) * numerator - Math.PI / 2),
			Math.sin(((2 * Math.PI) / denominator) * numerator - Math.PI / 2)
		];
	}

	$: minuteMark = {
		start: [
			getMarkNormal(date.getMinutes(), 60)[0] + 128,
			getMarkNormal(date.getMinutes(), 60)[1] + 128
		],
		end: [
			getMarkNormal(date.getMinutes(), 60)[0] * 120 + 128,
			getMarkNormal(date.getMinutes(), 60)[1] * 120 + 128
		]
	};

	let iconData: any;

	onMount(() => {
		date = new Date(Date.now());

		iconData = btoa(unescape(encodeURIComponent(new XMLSerializer().serializeToString(icon))));

		setInterval(() => {
			date = new Date(Date.now());

			iconData = btoa(unescape(encodeURIComponent(new XMLSerializer().serializeToString(icon))));

			if (timer % intervalLength === 0 && timer > 0) {
				new Notification(time12, {
					body: `${intervalsPassed} interval${intervalsPassed > 1 ? 's' : ''} [${Math.floor(
						timer / 60
					)} minutes]`,
					icon: `data:image/svg+xml;base64,${iconData}`
				});

				playAlarm(intervalsPassed);
			}
		}, 1000);
	});

	async function playAlarm(repeats: number) {
		if (browser && volume > 0.0 && muted === false) {
			const sound = new Audio('/alarm.wav');
			sound.volume = volume;

			await new Promise((resolve) => sound.addEventListener('canplaythrough', () => resolve(true)));

			let alarmCount = 0;
			while (alarmCount < repeats) {
				sound.play();

				await new Promise((resolve) => sound.addEventListener('ended', () => resolve(true)));

				alarmCount += 1;
			}
		}
	}

	let volume = 1.0;
	let muted = false;
	function toggleMute() {
		muted = !muted;
		if (!muted) {
			playAlarm(1);
		}
	}
</script>

<svelte:head>
	<title>Time Glasses</title>
	<link rel="icon" href={`data:image/svg+xml;base64,${iconData}`} />
</svelte:head>

<main>
	<section id="local">
		<header>Local Time</header>
		<h1 class="en-hour">
			{time12}
		</h1>
		<h1 class="meridian">{date.getHours() < 12 ? 'AM' : 'PM'}</h1>
		<h1 class="intl-hour">{time24}</h1>
	</section>

	<section id="timer">
		<header>Notification Timer</header>
		<h1>{timerDisplay}<small>{timerUnits}</small></h1>

		<div class="buttons" aria-label="timer controls">
			<button
				on:click={() => startTimer(900)}
				aria-pressed={fifteenMinuteTimerActive}
				aria-label="start timer with 15 minute notifications"
			>
				{#if !fifteenMinuteTimerActive}▶{:else}↻{/if}&ensp;15m
			</button>
			<button
				on:click={() => startTimer(300)}
				aria-pressed={fiveMinuteTimerActive}
				aria-label="start timer with 5 minute notifications"
			>
				{#if !fiveMinuteTimerActive}▶{:else}↻{/if}&ensp;5m
			</button>
			<button on:click={() => stopTimer()} aria-pressed={!timerRunning} aria-label="stop timer"
				>⯀</button
			>
		</div>

		<div aria-label="timer volume" id="volume">
			<button on:click={toggleMute}>
				{#if !muted}
					<img src="/unmuted.svg" alt="unmuted" />
				{:else}
					<img src="/muted.svg" alt="muted" />
				{/if}
			</button>
			<input
				type="range"
				bind:value={volume}
				on:change={() => playAlarm(1)}
				min="0.0"
				max="1.0"
				step="0.01"
				disabled={muted}
			/>
		</div>
	</section>

	<section id="utc">
		<header>UTC Time</header>
		<h1 class="utc-hour">{timeUTC}</h1>
		<h1 class="seconds">:{timeUTCSeconds}</h1>
	</section>

	<section id="brand">
		<h1>Time Glasses</h1>
	</section>

	<svg viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg" bind:this={icon}>
		<circle cx="128" cy="128" r="127" fill="#123" />

		<path d={minuteArc} fill="#888" style:translate="128px 128px" />
		<line
			x1={minuteMark.start[0]}
			y1={minuteMark.start[1]}
			x2={minuteMark.end[0]}
			y2={minuteMark.end[1]}
			stroke="black"
			stroke-width="30px"
			stroke-linecap="square"
		/>
		<line
			x1={minuteMark.start[0]}
			y1={minuteMark.start[1]}
			x2={minuteMark.end[0]}
			y2={minuteMark.end[1]}
			stroke="#ff0"
			stroke-width="15px"
			stroke-linecap="round"
		/>
	</svg>
</main>

<style>
	:global(:root) {
		font-family: 'Inter', system-ui, Arial, sans-serif;
	}
	@supports (font-variation-settings: normal) {
		:root {
			font-family: 'Inter var', system-ui, Arial, sans-serif;
		}
	}
	:global(body) {
		margin: 0;
	}

	h1 {
		margin: 0;

		text-align: center;
		line-height: 1;
		font-size: 60px;
		font-weight: 900;
		font-variant-numeric: tabular-nums;
	}
	h1 small {
		margin-left: 10px;
		font-size: 30px;
	}

	h2 {
		margin: 0;

		text-align: center;
		font-family: system-ui, sans-serif;
		font-size: 30px;
		font-weight: 600;
		font-variant-numeric: tabular-nums;
	}

	main {
		display: grid;
		grid-template-columns: repeat(2, 50vw);
		grid-template-rows: repeat(2, minmax(300px, 50vh));

		color: white;
	}
	section {
		position: relative;
		display: flex;
		flex-direction: column;
		justify-content: center;
	}
	#local {
		justify-content: space-evenly;
	}
	#timer {
		gap: 10px;
	}

	#local {
		background-color: #35373e;
	}
	#timer {
		background-color: #ff7a74;
	}
	#utc {
		background-color: #04a9bb;
	}
	#brand {
		background-color: #000067;
	}

	section header {
		position: absolute;
		top: 0;

		padding: 6px 8px 8px;

		font-size: 22px;
		font-weight: 200;
		line-height: 1;

		text-decoration: underline;
		text-decoration-thickness: 1px;
		text-decoration-color: #fff7;
		text-underline-offset: 4px;

		background-color: #151515;
	}

	.buttons {
		align-self: center;
	}
	.buttons button {
		margin-inline: auto;
		padding: 5px 15px;

		font-size: 20px;
		font-weight: 600;
		line-height: 1;
		color: white;

		background: none;
		border: 4px solid white;
		border-radius: 30px;
	}
	button[aria-pressed='true'] {
		background-color: white;
		color: #ff7a74;
	}

	#volume {
		position: absolute;
		bottom: 10px;
		left: 20px;

		display: flex;
		flex-direction: row;
	}
	#volume button {
		background: none;
		border: none;
	}

	#local,
	#utc {
		display: grid;
		grid-template-columns: 1fr min-content 1fr;
		align-content: center;
	}
	.en-hour,
	.utc-hour {
		grid-area: 1 / 2;
		justify-self: end;
	}
	.meridian,
	.seconds {
		grid-area: 1 / 3;
		justify-self: start;
		align-self: end;
	}
	.meridian {
		margin-left: 0.4em;
		margin-bottom: 5px;

		font-size: 35px;
	}
	.seconds {
		font-weight: 200;
		color: #333;
	}
	.intl-hour {
		grid-area: 2 / 2;
		opacity: 0.35;
	}

	#brand h1 {
		background-color: #220;
		margin-inline: auto;
		padding: 10px 20px;
	}

	svg {
		position: absolute;
		height: 256px;
		z-index: -1;
	}

	@media (max-width: 799px) {
		main {
			grid-template-columns: 100vw;
			grid-template-rows: repeat(4, minmax(300px, 30vh));
		}

		#timer {
			grid-row: 1;
		}
	}

	/** range input styling */
	input[type='range'] {
		width: 100%;
		margin: auto 0;
		background-color: transparent;
		-webkit-appearance: none;
		position: relative;
		top: -1.5px;
	}
	input[type='range']:focus {
		outline: none;
	}
	input[type='range']:disabled {
		opacity: 0.4;
	}
	input[type='range']::-webkit-slider-runnable-track {
		background: #ffffffaa;
		border: 0;
		width: 100%;
		height: 2px;
	}
	input[type='range']::-webkit-slider-thumb {
		margin-top: -9px;
		width: 6px;
		height: 20px;
		background: #ffffff;
		border: none;
		-webkit-appearance: none;
		cursor: pointer;
	}
	input[type='range']:focus::-webkit-slider-runnable-track {
		background: #ffffff;
	}
	input[type='range']::-moz-range-track {
		background: #ffffffaa;
		border: none;
		width: 100%;
		height: 2px;
	}
	input[type='range']::-moz-range-thumb {
		width: 6px;
		height: 20px;
		background: #ffffff;
		border: none;
		border-radius: 0;
		cursor: pointer;
	}
</style>
